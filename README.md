# Centro de estudiantes

## Introdución:

Este repositorio está orientado a la creación de una de las partes de la página oficial de EPET Nº20 (centro estudiantes).

## Qué es este repositorio?

Todo esto fue escrito en el lenguaje Python con ayuda de los frameworks Django y Django Rest Framework.

## Para instalar dependencias

> pip install -r requirements.txt
