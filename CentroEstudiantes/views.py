from rest_framework import viewsets
from django.contrib.auth.views import LoginView, LogoutView
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.views.generic import CreateView, TemplateView
from .serializers import ReunionSerializers
from .models import Reunion, PerfilUsuario
from .forms import RegistrateForm, ReunionForm


class RegistrateView(CreateView):
    model = PerfilUsuario
    form_class = RegistrateForm
    success_url = '/'
    template_name = 'PerfilUsuario/form_usuario.html'

    def form_valid(self, form):
    	form.save()
    	usuario = form.cleaned_data.get('username')
    	password = form.cleaned_data.get('password')
    	usuario = authenticate(username=usuario, password=password)
    	login(self.request, usuario)
    	return redirect('/')

class BienvenidaView(TemplateView):
   template_name = 'PerfilUsuario/bienvenida.html'

class InicioSesionView(LoginView):
	template_name = 'PerfilUsuario/login.html'

class CerrarSesionView(LogoutView):
	pass

class ReunionView(CreateView):
	model = Reunion
	form_class = ReunionForm
	success_url = '/'
	template_name = 'PerfilUsuario/reunion.html'
