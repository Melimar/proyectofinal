from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class PerfilUsuario(models.Model):
	nombre = models.CharField('Nombre', max_length=40, help_text='Ingrese su nombre')
	apellido = models.CharField('Apellido', max_length=40, help_text='Ingrese su apellido')
	usuario = models.OneToOneField(User, on_delete=models.CASCADE)

	def __str__(self):
		return '%s', '%s', '%s' % (self.nombre, self.apellido, self.usuario)
	
	class Meta:
		verbose_name = 'Perfil del usuario'

@receiver(post_save, sender=PerfilUsuario)
def crear_perfil_usuario(sender, instance, created, **kwargs):
    if created:
        PerfilUsuario.objects.create(usuario=instance)

@receiver(post_save, sender=PerfilUsuario)
def guardar_perfil_usuario(sender, instance, **kwargs):
    instance.perfil.save()

class Reunion(models.Model):
	titulo = models.CharField('Tema principal de reunion', max_length=40, help_text='Ingrese una breve descripcion')
	fecha = models.DateTimeField('Fecha y hora en la que se realizara la reunion')
	asunto = models.TextField('Tema detallado a tratar en la reunion', max_length=220, help_text='Ingrese una descripcion detallada')

	def __str__(self):
		return '%s', '%s', '%s' % (self.titulo, self.fecha, self.asunto)

	class Meta:
		verbose_name_plural = 'Reuniones'
