from django.urls import path
from .views import ReunionView, RegistrateView, BienvenidaView, LoginView, CerrarSesionView


urlpatterns = [
    path('reunion/', ReunionView.as_view(), name='reunion'),
    path('', BienvenidaView.as_view(), name='bienvenida'),
    path('registrate/', RegistrateView.as_view(), name='registrate'),
    path('login/', LoginView.as_view(), name='inicia_sesion'),
    path('cerrar_sesion', CerrarSesionView.as_view(), name='cerrar_sesion'),
]
