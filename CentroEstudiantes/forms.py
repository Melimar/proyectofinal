from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from .models import PerfilUsuario, Reunion

class RegistrateForm(UserCreationForm):
    nombre = forms.CharField(max_length=140, required=True)
    apellido = forms.CharField(max_length=140, required=False)

    class Meta():
        model = User
        fields = ['username', 'email', 'first_name', 'last_name', 'password1', 'password2']

class ReunionForm(forms.ModelForm):
	class Meta:
		model = Reunion
		fields = ['titulo', 'fecha', 'asunto']