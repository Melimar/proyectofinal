from django.contrib import admin
from .models import Reunion, PerfilUsuario


@admin.register(Reunion)
class ReunionAdmin(admin.ModelAdmin):
	model = Reunion

@admin.register(PerfilUsuario)
class PerfilAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apellido', 'usuario')
