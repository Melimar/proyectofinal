from django.apps import AppConfig


class CentroestudiantesConfig(AppConfig):
    name = 'CentroEstudiantes'
