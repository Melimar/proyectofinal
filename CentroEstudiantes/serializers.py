from rest_framework import serializers
from .models import Reunion


class ReunionSerializers(serializers.ModelSerializer):
	class Meta:
		model = Reunion
		fields = ['tema', 'fecha', 'asunto', 'dirigido']
